.. currentmodule:: sardana.pool.poolsynchronization

:mod:`~sardana.pool.poolsynchronization`
========================================

.. automodule:: sardana.pool.poolsynchronization

.. rubric:: Classes

.. hlist::
    :columns: 3

    * :class:`PoolSynchronization`
    * :class:`SynchDescription`
    * :class:`MultipleSynchDescription`

PoolSynchronization
-------------------

.. inheritance-diagram:: PoolSynchronization
    :parts: 1
    
.. autoclass:: PoolSynchronization
    :show-inheritance:
    :members:
    :undoc-members:

SynchDescription
----------------

.. inheritance-diagram:: SynchDescription
    :parts: 1

.. autoclass:: SynchDescription
    :show-inheritance:
    :members:
    :undoc-members:

MultiSynchDescription
---------------------

.. inheritance-diagram:: MultiSynchDescription
    :parts: 1

.. autoclass:: MultiSynchDescription
    :show-inheritance:
    :members:
    :undoc-members:
